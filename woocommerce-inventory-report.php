<?php
/*
Plugin Name: WooCommerce Inventory Report 
Plugin URI: https:bunzwebdesign.com
Description: Add the ability to see monetary value of stock products.
Version: 1.0 
Author: Fabunde Mamey
Author URI: https:bunzwebdesign.com/about-us
Text Domain: woocommerce-inventory-report
Domain Path: /languages
*/
$fm_total_dollar_value = 0;
// check to make sure if Woocommerce is active
if( in_array( 'woocommerce/woocommerce.php', apply_filters('active_plugins', get_option('active_plugins')))){
	// only runs if there's no class with this name
	if( ! class_exists('WC_Inventory_Extension')){
		class WC_Inventory_Report{
			public function __construct(){
				add_filter('woocommerce_settings_tabs_array', array( $this, 'add_report_tab'), 50);

				add_action('woocommerce_settings_inventory_report', array( $this, 'display_report_table'), 50);
			}

			// added to settings tab
			// TODO: needs to be added to Woocommerce Invetory tab
			public function add_report_tab($settings_tabs){
				$settings_tabs['inventory_report'] = __('Invetory Report', 'woocommerce-inventory-report');
				return $settings_tabs;
			}

			// display table 
			public function display_report_table(){
				// woocommerce_admin_fields( self::get_settings() );

				// Get all products.
				$args = array(
				    'limit' => 100,
				);
				$products = wc_get_products( $args );
				
				$totalStock = 0;
				$totalDollarValue = 0;
				$tableId = 0;

				?>
				<table class="wp-list-table widefat fixed striped stock">
					<thead>
						<tr>
							<th>#</th>
							<th>Name</th>
							<th>Price</th>
							<th>Stock</th>
							<th>Dollar Value</th>
						</tr>
					</thead>
					
				 	<tbody>
		 			<?php
		 			foreach ($products as $product) {
		 			//increment table Id
		 			$tableId ++;
		 		 	?>
					<tr>
						<td>
							<?php echo $tableId ?>
						</td>
						<td> 
							<?php 
						//show product name
						if($product->is_type( 'variable' )){
							    $variations = $product->get_available_variations();
							    // $fm_stock_count = 0;
							    foreach($variations as $variation){
							         $variation_id = $variation['variation_id'];
							         $variation_obj = new WC_Product_variation($variation_id);

							        echo "- " . $variation_obj->get_name() . "</br>";
							    }
							
						}else{
							echo $product->get_name(); 
						}

						?> 
					</td>

						<td> 
							<?php 
							// show product price
								if($product->is_type( 'variable' )){
								    $variations = $product->get_available_variations();
								    foreach($variations as $variation){
								         $variation_id = $variation['variation_id'];
								         $variation_obj = new WC_Product_variation($variation_id);
								         echo $variation_obj->get_regular_price() . "</br>";

								    }
								
							}else{
								    echo $product->get_regular_price();
								}
							?> 
						</td>

						<td> 
						<?php 
						// show stock
						if($product->is_type( 'variable' )){
							    $variations = $product->get_available_variations();
							    // $fm_stock_count = 0;
							    foreach($variations as $variation){
							         $variation_id = $variation['variation_id'];
							         $variation_obj = new WC_Product_variation($variation_id);
							         // $fm_stock_count = $fm_stock_count + $variation_obj->get_stock_quantity();
							         if ($variation_obj->get_stock_quantity() < 1) {
							         	echo "n/a";
							         }

							         echo $variation_obj->get_stock_quantity() . "</br>";
							         // add stock to count for variable products 
							         $totalStock += $variation_obj->get_stock_quantity();
							    }
							 // echo $fm_stock_count;
							
						}else{
							if ($product->get_stock_quantity() < 1) {
								echo "n/a";
							}
							echo $product->get_stock_quantity(); 
							// add stock to count for non variable products 
							$totalStock += $product->get_stock_quantity();
						}

						?> 
					</td>

						<td> 
							<?php 
							// Show dollar value 
							if($product->is_type( 'variable' )){
								    $variations = $product->get_available_variations();
								    // $fm_stock_count = 0;
								    foreach($variations as $variation){
								         $variation_id = $variation['variation_id'];
								         $variation_obj = new WC_Product_variation($variation_id);
								         // $fm_stock_count = $fm_stock_count + $variation_obj->get_stock_quantity();
								         $thisVariationStock = $variation_obj->get_stock_quantity();
								         $thisVariationPrice = $variation_obj->get_regular_price();
								         if( $thisVariationStock > 0 && $thisVariationPrice > 0){
								         	echo $thisVariationStock * $thisVariationPrice . "</br>";
								         	// add dollar value to total dollar value count for each variation.
								         	$totalDollarValue += $thisVariationStock * $thisVariationPrice;
								         }
								    }
								 // echo $fm_stock_count;
								
							}else{
								if($product->get_stock_quantity() > 0 && $product->get_regular_price() > 0 ){
									echo $product->get_stock_quantity() * $product->get_regular_price();
									// add dollar value to total dollar value count for non variation product.
									$totalDollarValue += $product->get_stock_quantity() * $product->get_regular_price();
								}
							}
						 ?> 
					</td>

					</tr>
					</tbody>
				<?php } // product foreach loop ends ?>

				<tfoot>
					<tr>
						<td>Total</td>
						<td><?php echo "Total $" ?></td>
						<td><?php echo $totalStock ?></td>
						<td><?php echo "Total $ Value " . $totalDollarValue ?></td>
					</tr>
				</tfoot>
				</table>
				<?php
			}



		}
		$GLOBALS['wc_inventory_report'] = new WC_Inventory_Report();
	}
}